
const express = require('express');

function add(req, res, next) {
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`${n1}+${n2} = ${n1+n2}`);
}

function multiply(req, res, next) {
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    res.send(`${n1}*${n2} = ${n1*n2}`);
}

function split(req, res, next) {
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    res.send(`${n1}/${n2} = ${n1/n2}`);
}

function power(req, res, next) {
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    res.send(`${n1}^${n2} = ${Math.pow(n1, n2)}`);
}

function subtract(req, res, next) {
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    res.send(`${n1}-${n2} - ${n1-n2}`);
}

module.exports = {add,multiply,split,power,subtract};



